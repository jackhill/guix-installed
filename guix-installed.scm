(define-module (guix-installed)
  #:use-module (guix monads)
  #:use-module (guix profiles)
  #:use-module (guix store)
  #:use-module (guix-installed parser)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1)
  #:export (print-installed))

;;Glob for finding user profiles
(define %user-profiles-glob (string-append %gc-roots-directory
					 "/profiles/per-user/*/*"))

;;Glob for finding system profiles
(define %system-profiles-glob (string-append %gc-roots-directory
					    "/profiles/*/profile"))

;;A list of paths matched by glob
(define (glob->paths glob)
  (let ((p (open-input-pipe (string-append "ls -d " glob))))
    (let loop ((l (read-line p)))
      (if (eof-object? l)
	  (begin (close-input-port p) '())
	  (cons l
		(loop (read-line p)))))))

;;A list of items in the profiles in in input list
(define (profiles->items profiles)
  (concatenate
   (map (compose (lambda (lst) (map manifest-entry-item lst))
		 manifest-entries
		 profile-manifest)
	profiles)))

;;Return a monadic procedure that calculates the list of
;;references for a list of store items
(define (references-m items)
  (lambda (store)
    (values (requisites store items)
	    store)))

(define (print-item path)
  (let ((parse (lambda () (parse-store path)))
	(print (lambda (n h) (simple-format #t "~a\t~a\n" n h))))
    (call-with-values parse print)))

;;The entrypoint that prints the list of installed packages
;;and their store-hashes
(define (print-installed)
  (let* ((user-profiles (glob->paths %user-profiles-glob))
	 (system-profiles (glob->paths %system-profiles-glob))
	 (all-profiles (append user-profiles system-profiles))
	 (items (profiles->items all-profiles))
	 (store-paths (run-with-store (open-connection) (references-m items))))
    (for-each print-item store-paths)))
