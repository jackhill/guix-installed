(define-module (guix-installed parser)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  #:export (parse-store))

(define-peg-string-patterns
  "item <-- prefix hash DASH name
prefix < '/gnu/store/'
hash <-- hashchar+
hashchar <- [0-9a-z]
name <-- .+
DASH < '-'")

(define (match-parse p)
  (match p
    ((item (hash h) (name n))
     (values n h))))

(define (parse-store path)
  (let ((p (peg:tree (match-pattern item path))))
    (match-parse p)))
