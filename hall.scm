(hall-description
  (name "guix-installed")
  (email "jackhill@duke.edu")
  (prefix "")
  (version "0.0.1")
  (author "")
  (copyright (2019))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `())
  (skip ())
  (files (libraries
           ((directory
              "guix-installed"
              ((scheme-file "parser")))
            (scheme-file "guix-installed")))
         (tests ((directory "tests" ())))
         (programs
           ((directory
              "scripts"
              ((in-file "guix-installed")))))
         (documentation
           ((directory "doc" ((texi-file "guix-installed")))
            (text-file "COPYING")
            (text-file "HACKING")
            (text-file "README")))
         (infrastructure
           ((scheme-file "hall") (scheme-file "guix")))))
